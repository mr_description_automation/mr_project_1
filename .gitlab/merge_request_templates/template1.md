## Bug Fix Merge Request

### Description

**Summary:**

A brief summary of the bug being fixed.

**Issue:**

Closes #XXX (replace XXX with the issue number this MR fixes)

### Changes Made

Describe the changes made to fix the bug. Include technical details, code snippets, or any relevant information.

### Testing

Describe the testing performed to verify the bug fix. Include steps to reproduce the issue before the fix and steps to verify that the issue is resolved after applying the fix.

### Related Issues

List any related issues or dependencies that this bug fix is linked to.

### Screenshots (if applicable)

Include screenshots or images that visually demonstrate the bug and its resolution.

### Checklist

- [ ] I have reviewed the code changes.
- [ ] I have tested the bug fix locally.
- [ ] The automated tests (if any) pass.
- [ ] I have updated the documentation (if necessary).
- [ ] I have assigned this MR to the appropriate reviewer.

### Reviewer Checklist

- [ ] The code changes look good to me.
- [ ] The bug fix has been tested and verified.
- [ ] The documentation (if updated) is accurate.
- [ ] The code follows our coding standards.

### Definition of Done

- [ ] The bug is fixed and verified.
- [ ] The code is reviewed and approved by a colleague.
- [ ] All checklist items are completed.
- [ ] The merge request is linked to the relevant issue.

## Test Plan

Outline a test plan for further testing, including any specific test cases or scenarios that need to be validated.

## Additional Notes

Include any additional notes or information that may be relevant to this bug fix.
