#!/bin/bash

# Prompt the user for GitLab Personal Access Token
read -p "Enter your GitLab Personal Access Token: " TOKEN

# Prompt the user for GitLab Project ID
read -p "Enter your GitLab Project ID: " PROJECT_ID

# Prompt the user for the source branch
read -p "Enter the source branch: " SOURCE_BRANCH

# Prompt the user for the target branch
read -p "Enter the target branch: " TARGET_BRANCH

echo "$PROJECT_ID"
echo "$SOURCE_BRANCH"
echo "$TARGET_BRANCH"

# List available merge request templates in the .gitlab/merge_request_templates directory
templates_dir=".gitlab/merge_request_templates"
templates=($(ls -1 "$templates_dir"/*.md))

# Check if there are any templates available
if [ ${#templates[@]} -eq 0 ]; then
  echo "No merge request templates found in the $templates_dir directory."
  exit 1
fi

# Prompt the user to select a template
echo "Select a merge request template:"
select template_file in "${templates[@]}"; do
  if [ -n "$template_file" ]; then
    # Read the content of the selected template file
    template_content=$(cat "$template_file")

    # Create the merge request using the selected template
    echo "Creating a merge request with the selected template..."
    curl --request POST \
      --header "PRIVATE-TOKEN: $TOKEN" \
      --header "Content-Type: application/json" \
      --data '{
        "source_branch": "'"$SOURCE_BRANCH"'",
        "target_branch": "'"$TARGET_BRANCH"'",
        "title": "Merge Request Title",
        "description": "'"$template_content"'",
        "target_project_id": '$PROJECT_ID'
      }' \
      "https://gitlab.com/api/v4/projects/$PROJECT_ID/merge_requests"
    echo "Merge request created successfully."
    exit 0
  else
    echo "Invalid selection. Please choose a valid template."
  fi
done

