#!/bin/bash

# Prompt the user for GitLab Personal Access Token
read -p "Enter your GitLab Personal Access Token: " TOKEN

# Prompt the user for GitLab Project ID
read -p "Enter your GitLab Project ID: " PROJECT_ID

# Prompt the user for the source branch
read -p "Enter the source branch: " SOURCE_BRANCH

# Prompt the user for the target branch
read -p "Enter the target branch: " TARGET_BRANCH

# Specify the template file (e.g., bug_fix.md)
TEMPLATE_FILE=".gitlab/merge_request_templates/template1.md"

# Read the template content from the file
TEMPLATE_CONTENT=$(cat "$TEMPLATE_FILE")

# Set your GitLab API URL
GITLAB_API_URL="https://gitlab.com/api/v4"

# Create the merge request using the template content
curl --request POST \
  --header "PRIVATE-TOKEN: $TOKEN" \
  --header "Content-Type: application/json" \
  --data "{
    \"source_branch\": \"$SOURCE_BRANCH\",
    \"target_branch\": \"$TARGET_BRANCH\",
    \"title\": \"Merge Request Title\",
    \"description\": \"$TEMPLATE_CONTENT\",
    \"target_project_id\": $PROJECT_ID
  }" \
  "$GITLAB_API_URL/projects/$PROJECT_ID/merge_requests"
